# Simple ERC20 Explorer
This app shows all historical ERC20 transactions for a searched address.  
All displayed data is retrieved from the etherscan.io API.    
Since this is free and publicly exposed API, larger search requests are throttled; all requests timeout after 3 seconds.  

## Tools
This project was made using Angular 7, implementing Material design.  
API provided by ethererscan.io.  
CI/CD & version control provided by Gitlab.  
The web app is hosted by Firebase.

## Links
Web App: https://simple-erc20-explorer.web.app/explorer  
API: https://etherscan.io/apis#accounts  
Gitlab: https://gitlab.com/emclaug2/simple-ethereum-explorer

## Contributors
This project was made by Evan McLaughlin.   

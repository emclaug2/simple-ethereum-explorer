import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent {

  @Input() isMobile: boolean;
  @Output() search: EventEmitter<string> = new EventEmitter();
  constructor() { }

  select(address: string): void {
    this.search.emit(address);
  }
}

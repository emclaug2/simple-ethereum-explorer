import {Transaction} from './Transaction';

export class ApiResponse {
  constructor(public status: string,
              public message: string,
              public result: Transaction[]) {}
}

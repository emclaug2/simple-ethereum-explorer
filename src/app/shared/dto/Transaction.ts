export class Transaction {
  constructor(
    public blockhash: string,
    public blockNumber: string,
    public confirmations: string,
    public contractAddress: string,
    public from: string,
    public gas: string,
    public gasPrice: string,
    public hash: string,
    public input: string,
    public nonce: string,
    public timeStamp: string,
    public to: string,
    public tokenDecimal: string,
    public tokenName: string,
    public tokenSymbol: string,
    public transactionIndex: string,
    public value: string) {}
}

